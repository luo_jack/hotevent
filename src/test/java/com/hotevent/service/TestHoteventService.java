package com.hotevent.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestHoteventService {

    @Autowired
    HoteventService hoteventService;

    @Test
    public void test1(){
        System.out.println(hoteventService.getBykeyWordPageCount("",2));
    }

    @Test
    public void test2(){
        System.out.println(hoteventService.getBykeyWord("",2,3));
    }
}
