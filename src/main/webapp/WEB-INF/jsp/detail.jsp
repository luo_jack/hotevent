<%--
  Created by IntelliJ IDEA.
  User: 17400
  Date: 2022/2/25
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>事件详情</title>
    <style>
        body{

        }
        .body{
            width: 800px;
            margin: 0 auto;
        }
        .comment{
            background: skyblue;
            margin: 20px 0;
            padding: 5px;
            line-height: 1.5em;
        }
    </style>
</head>
<body>
    <div class="body">
        <h2>${hotevent.keyWord}</h2>
        <p>创建于: ${hotevent.createDate}</p>
        <p>
            ${hotevent.hotContent}
        </p>
        <h3>评论</h3>
        <c:forEach items="${comments}" var="item">
            <div class="comment">
                评论事件: ${item.commentDate}<br/>
                ${item.content}
            </div>
        </c:forEach>

        <hr/>
        <form action="../addComment" method="post">
            快速评论
            <div>
                <textarea name="content" cols="30" rows="3"></textarea>
            </div>
            <input type="hidden" name="hotEventsId" value="${hotevent.id}"/>
            <input type="submit"/><a href="../index">返回首页</a>
        </form>
    </div>
</body>
</html>
