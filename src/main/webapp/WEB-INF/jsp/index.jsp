<%--
  Created by IntelliJ IDEA.
  User: 17400
  Date: 2022/2/25
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>排行榜</title>
    <style>
        body{
            text-align: center;
        }
        table{
            border: 1px solid black;
            margin: 0 auto;
            width: 680px;
            margin-bottom: 20px;
        }
        td{
            text-align: center;
        }
    </style>
</head>
<body>
    <form action="index" method="get">
        关键字: <input type="text" name="keyWord" value="${keyWord}"/>
        <input type="submit" value="查询"/>
    </form>
    <h1>热点事件排行榜</h1>
    <table border="1">
        <thead>
            <tr>
                <th>关键字</th>
                <th>搜索指数</th>
                <th>创建事件</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${hotevents}" var="item">
                <tr>
                    <td><a href="detail/${item.id}">${item.keyWord}</a></td>
                    <td>${item.searchNum}</td>
                    <td>${item.createDate}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <div>
        <a href="index?keyWord=${keyWord}&startPage=1">首页</a>
        <a href="index?keyWord=${keyWord}&startPage=${startPage-1}">上一页</a>
        <a href="index?keyWord=${keyWord}&startPage=${startPage+1}">下一页</a>
        <a href="index?keyWord=${keyWord}&startPage=${pageCount}">末页</a>
        第 ${startPage} 页 / 第 ${pageCount} 页
    </div>
</body>
</html>
