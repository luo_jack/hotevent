package com.hotevent.dao;

import com.hotevent.entity.Hotevent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface HoteventDao {

    Hotevent getOne(@Param("id") int id);

    List<Hotevent> getBykeyWord(@Param("keyWord") String keyWord,
                                @Param("start") int start,
                                @Param("limit") int limit);

    int getBykeyWordCount(@Param("keyWord") String keyWord);
}
