package com.hotevent.dao;

import com.hotevent.entity.Comment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CommentDao {

    List<Comment> getByEventId(int hoteventId);

    int add(Comment comment);
}
