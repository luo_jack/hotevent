package com.hotevent.controller;

import com.hotevent.entity.Hotevent;
import com.hotevent.service.CommentService;
import com.hotevent.service.HoteventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/restevent")
public class RestHoteventController {

    @Autowired
    HoteventService hoteventService;

    //每页行数
    int pageRowsCount = 3;

    @GetMapping("/query")
    public Object queryBykeyWord(@RequestParam(value = "keyWord",
                                        required = false,
                                        defaultValue = "") String keyWord,
                               @RequestParam(value = "startPage",
                                       required = false,
                                       defaultValue = "1") int startPage) {
        HashMap<String,Object> map = new HashMap<>();
        int pageCount = hoteventService.getBykeyWordPageCount(keyWord,pageRowsCount);
        if(startPage<1){
            startPage = 1;
        }else if(startPage > pageCount){
            startPage = pageCount;
        }
        List<Hotevent> hotevents
                = hoteventService.getBykeyWord(keyWord,startPage,pageRowsCount);
        map.put("code",200);
        map.put("hotevents",hotevents);
        map.put("pageCount",pageCount);
        map.put("startPage",startPage);
        map.put("keyWord",keyWord);
        return map;
    }

    @GetMapping("/one/{id}")
    public Object getOne(@PathVariable("id") int id){
        HashMap<String,Object> map = new HashMap<>();
        Hotevent hotevent = hoteventService.getOne(id);
        if(hotevent == null){
            map.put("code",400);
            map.put("message","没有相关对象");
        }else{
            map.put("code",200);
            map.put("message","");
        }
        map.put("data",hotevent);
        return map;
    }
}
