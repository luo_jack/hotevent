package com.hotevent.controller;

import com.hotevent.entity.Comment;
import com.hotevent.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/restcomment")
public class RestCommentController {

    @Autowired
    CommentService commentService;

    @GetMapping("/comments/{eventId}")
    public Object getByEventId(@PathVariable("eventId") int eventId){
        HashMap<String,Object> map = new HashMap<>();
        map.put("code",200);
        List<Comment> comments = commentService.getByEventId(eventId);
        map.put("data",comments);
        return map;
    }

    @PostMapping("/add")
    public Object addComment(Comment comment){
        HashMap<String,Object> map = new HashMap<>();
        comment.setCommentDate(new Date(System.currentTimeMillis()));
        commentService.add(comment);
        map.put("code",200);
        return map;
    }
}
