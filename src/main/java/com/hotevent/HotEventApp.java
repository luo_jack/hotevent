package com.hotevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotEventApp {

    public static void main(String[] args) {
        SpringApplication.run(HotEventApp.class,args);
    }
}
