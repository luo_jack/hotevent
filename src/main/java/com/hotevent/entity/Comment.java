package com.hotevent.entity;

import lombok.*;

import java.sql.Date;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    private int id;

    private int hotEventsId;

    private Date commentDate;

    private String content;
}
