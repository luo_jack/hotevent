package com.hotevent.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Date;

@Data
@Builder
@ToString
public class Hotevent {

    private int id;

    private String keyWord;

    private String hotContent;

    private int searchNum;

    private Date createDate;
}
