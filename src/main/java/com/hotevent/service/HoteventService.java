package com.hotevent.service;

import com.hotevent.dao.HoteventDao;
import com.hotevent.entity.Hotevent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HoteventService {

    @Autowired
    HoteventDao hoteventDao;

    public Hotevent getOne(int id){
        return hoteventDao.getOne(id);
    }

    //返回页数
    public int getBykeyWordPageCount(String keyWord,int pageRowsCount){
        int count = hoteventDao.getBykeyWordCount(keyWord);
        if(count % pageRowsCount == 0){
            return count / pageRowsCount;
        }else{
            return count / pageRowsCount + 1;
        }
    }

    public List<Hotevent> getBykeyWord(String keyWord,
                                       int startPage,
                                       int limit){
        int start = (startPage-1) * limit;
        return hoteventDao.getBykeyWord(keyWord,start,limit);
    }
}
