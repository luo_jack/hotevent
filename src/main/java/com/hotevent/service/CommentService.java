package com.hotevent.service;

import com.hotevent.dao.CommentDao;
import com.hotevent.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentDao commentDao;

    public List<Comment> getByEventId(int hoteventId){
        return commentDao.getByEventId(hoteventId);
    }

    public int add(Comment comment){
        return commentDao.add(comment);
    }
}
